# TeensyMIDIAudio

(c)2018 H.Wirtz

A simple (modular) PCB for using a Teensy-3.6 (or 3.5) for making music.

## License

CC BY-NC-ND 3.0 (see https://creativecommons.org/licenses/by-nc-nd/3.0/)

## Description

This PCB is a summary of several circuits for the Teensy. It adds the following
features to one board:

* MIDI I/O with DIN connectors
* Audio I/O with the Teensy-Audio-Board
* alternative solution: Audio output with the PT8211
* headers for analog and digital I/O (for all pins which are not used for the
above features), e.g. for connecting rotary encoders, potentiometers, switches,
...
* headers for 3.3V, 5V and GND
* connector and header for USB host (only Teensy-3.6, e.g. using as MIDI port)
* connectors for audio output (and input when using Teensy-Audio-Board)
* USB-B connector for supplying power
* I2C header with pullup resistors (which can be disabled)

You need not equip all parts if you don't want to use all the features.

TeensyMIDIAudio-3d.png
[![3D example view](https://about.teahub.io/dcoredump/TeensyMIDIAudio/raw/branch/master/TeensyMIDIAudio-3d.png)](https://about.teahub.io/dcoredump/TeensyMIDIAudio/raw/branch/master/TeensyMIDIAudio-3d.png)

## Idea behind

This is my first creation of a complex board and my first time using KICAD
(http://kicad-pcb.org/). I created this PCB for having a flexible
microcontroller-synth-plattform. The first implementation for this is a port
of the DX-7 emulation of Dexed called MicroDexed
(https://about.teahub.io/dcoredump/MicroDexed). First I used a stripe board as
a prototype and stackable connectors for the Teensy-3.6 and the
Teensy-Audio-Board. After trying around I found out that a simple LCD display
and an encoder would be helpful to change banks and sounds. So I decided to
create a better plattform for this and created a flexible PCB.

The same PCB should make availabe a cheap and simple or more complex construction
on top of the same hardware base. So you have the option to simply use the
PT8211 (with only audio outputs) and the onboard USB-MIDI as base for a
synth or a full armed version with DIN-MIDI, encoders, LCD display and Audio
input and output.

You can build MIDI playable synths, effects or just sound generators - it's your
part to decide what to do. For this the Teensy is really a very good platform.
You can use the Arduino-IDE and several very good libraries especially for the
audio stacks. Also there are many third party libraries for nearly every 
hardware which exists - or you can decide to write your own library.

The size of the board is for using it inside a Rittal CMC-TC case. I have lots
of them which can be recycled as cases for this project.

## Choose your modules / BOM

[![Module view](https://about.teahub.io/dcoredump/TeensyMIDIAudio/raw/branch/master/TeensyMIDIAudio-3d.png)](https://about.teahub.io/dcoredump/TeensyMIDIAudio/raw/branch/master/TeensyMIDIAudio-3d.png)

You can choose which parts you need for the thing your board should do. The board
is kind of modular, so there are only some parts you need every time:

* 1 * USB-B-socket (J1)
* 1 * Capacitor 100nF (C1)
* 1 * 3-pin header (JP3)
* 1 * Jumper for JP3
* 1 * 48-pin IC socket (or two 24-pin-rows)
* 1 * Teensy-3.6 (or Teensy-3.5)

(A Teensy-3.5 also works, but you don't have the option for USB-MIDI and it has less CPU power!)

Now you can add optional parts which fit your personal design best:

### [A] DIN-MIDI-IN
* 1 * 8-pin IC socket (U5)
* 1 * 6N128 opto-coupler (pacle in U5)
* 1 * 1N4148 (or similar) diode (D1)
* 1 * 470 ohm resistor R1
* 1 * 220 ohm resistor R5
* 1 * 5-pin-DIN socket (J15)

### [B] DIN-MIDI-OUT
* 2 * 47 ohm resistor (R3, R4)
* 1 * 5-pin-DIN socket (J14)

### [C] USB-MIDI (only Teensy-3.6, additionaly or instead of DIN-MIDI)
* 1 * vertical USB-A-socket (J13)
* 1 * 4-pin header (J10)

### [D] Audio out
* 1 * vertical stereo RCA sockets (J11)
* 1 * 4-pin header (J12)

### [E] Audio in (only with Teensy-Audio-Board)
* 1 * vertical stereo RCA sockets (J9)
* 1 * 3-pin header (J8)

### [F] Teensy-Audio-Board (HQ audio in- and output)
* 1 * 24-pin IC socket (U2)
* 1 * 3-pin header (J12)
* 1 * Teensy-Audio-Board

### [G] PT8211 (only audio output, limited quality and higher CPU/memory usage)
* 2 * 2-pin header (JP1, JP2)
* 1 * 10 ohm resistor (R2)
* 3 * 47uF capcitor (C3, C4, C5)
* 1 * 100nF capacitor (C2)
* 2 * jumper for JP1, JP2
* 1 * PT8211

#### PT8211 as DIP or with a DIP adapter?
* 1 * Adafruit TO adapter PCB
* 1 * 8-pin IC socket (U4)

### [H] I2C for LCD display
* 1 * 4-pin header (J7)
* 2 * 2-pin headder (J4, J5)
* 2 * jumper for JP4 and JP5
* 2 * resistor 4.7K ohm (R6, R7)

### [I] Pin headers for 3.3V/GND
* 1 * 2-rows-4-pin header (J6)

### [J] Pin headers for 5V/GND
* 1 * 2-rows-4-pin header (J5)

### [K] Pin headers for digital I/O
* 1 * 2-rows-9-pin header (J2)

### [L] Pin headers for analog (or digital) I/O
* 1 * 2-rows-12-pin header (J3)

### [M] Pin headers for serial I/O
* 1 * 2-rows-4-pin header (J4)

## Example combinations of modules

Due to a modular layout there are several combinations you can choose from for best fitting your
project. You have to keep in mind that you can only use one audio board at a time. So you can add a
Teensy-Audio-Board (F) and a PT8211 (G) at the same time, but you have to choose which one is 
running with the jumpers JP1, JP2 and JP3. If the jumpers are located towards the audio out
RCA-connectors, the PT8211 is enabled. Otherwise (JP3 is located towards the PT8211 and JP1, JP2 are
open) the Teensy-Audio-Board is used.

The normal combination of modules for MicroDexed are the following:

### HQ audio out with DIN-MIDI:
* Base + A + B + D + F

### HQ audio out with USB-MIDI:
* Base + C + D + F

### HQ audio out with DIN-MIDI, USB-MIDI and LCD-display:
* Base + A + B + C + D + F + H

Combination as audio effect:

### HQ audio in and out:
* Base + D + E + F

Simple and "cheap" sound generator/synth:

### LQ audio out with DIN-MIDI-I/O
* Base + A + B + D + G

There are much more combinations available. You can also add some pin-headers if you additionaly want
to connect digital and/or analog I/O. All unused pins of the Teensy are routet towards the headers. 
The analog pins can also be used for digital I/O. The reference for the pin numbers is the electric
circuit schematic.

Some more hints:
* For connecting the audio out and/or in of the Teensy-Audio-Board you have to solder additional
pin-headers on top of the board. See https://www.pjrc.com/store/teensy3_audio.html. After that you have
to wire the pins towards the headers behind the RCA-connectors. For the audio out the connectors
arrangement is *left-ground-ground-right* and for audio in *left-ground-right*.
* For using USB-MIDI you have to use a Teensy-3.6. You have to solder pin-headers or wires onto the
Teensy-3.6-PCB (see https://www.pjrc.com/store/teensy36.html, *G-D+-D-5V*). Those pins must be
conected to the pin-header behind the vertical USB-A-socket (J10): *GND, D+, D-,5V*.
* If you want to add an I2C-LCD-display you can use the pin-header J7. If you need pullup resistors for
I2C you have to solder R6 and R7 and/or close the jumpers JP4 and JP5.
* Please note that the PT8211 has only audio output. Also this cheap IC does not have as good sound
quality as the SGTL5000 on the Teensy-Audio-Board (which has additional audio input).
* DIN-MIDI and USB-MIDI can be used at the same time if the sketch supports this (MicroDexed does).

## Sources used

* Teensy-Audio-Board: https://www.pjrc.com/store/teensy3_audio.html
* PT8211 circuit: https://www.pjrc.com/store/pt8211_kit.html
* Teensy MIDI circuit: https://www.pjrc.com/teensy/td_libs_MIDI.html
* Creating sound/effect stacks: https://www.pjrc.com/teensy/gui/

## Thanks

A big thanks for all who made Teensy and the software around working. Especially
the guys from the forum, which are very helpful and have patience with persons
who start with hardware and microcontroller business.

Also a big thanks for the KICAD guys - this software is really awesome.

And not to forget the Arduino guys - IMHO they are some of the founders of 
the idea of making "things" with microcontrollrs easier.

And, of course all Open-Source developers who let us take part of their work
by opening their code as free software.
